const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');


const Dishes = require('../models/dishes');

const dishRouter = express.Router();

dishRouter.use(bodyParser.json());

//routing '/'
dishRouter.options('/', cors.corsWithOptions, (req, res) => {
	res.sendStatus(200);
});

dishRouter.get('/', cors.cors, (req, res, next) => {
		Dishes.find(req.query)
			.populate('comments.author')
			.then((dishes) => {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.json(dishes);
			})
			.catch((err) => next(err));
	});

dishRouter.post('/', cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
		Dishes.create(req.body)
		.then((dish) => {
			console.log('Dish created', dish);
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			res.json(dish);
		})
		.catch((err) => next(err));
	});

dishRouter.put('/', cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('PUT method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});

dishRouter.delete('/', cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin,  (req, res, next) => {
		Dishes.remove({})
		.then((resp) => {
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			res.json(resp);
		})
		.catch((err) => next(err));
	});


//routing '/:dishId'
dishRouter.options('/:dishId', cors.corsWithOptions, (req, res) => {
	res.sendStatus(200);
});

dishRouter.get('/:dishId', cors.cors, (req, res, next) => {
		Dishes.findById(req.params.dishId)
			.populate('comments.author')
			.then((dish) => {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.json(dish);
			})
			.catch((err) => next(err));
	});

dishRouter.post('/:dishId', cors.corsWithOptions,  authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('POST method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});

dishRouter.put('/:dishId',  cors.corsWithOptions, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		Dishes.findByIdAndUpdate(req.params.dishId, {
			$set: req.body
		}, { new: true })
			.then((dish) => {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.json(dish);
			})
			.catch((err) => next(err));
	});

dishRouter.delete('/:dishId',  cors.corsWithOptions, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		Dishes.findByIdAndRemove(req.params.dishId)
		.then((resp) => {
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			res.json(resp);
		})
		.catch((err) => next(err));
	});


module.exports = dishRouter;
