const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');


const Favorites = require('../models/favorites');
const Dishes = require('../models/dishes');

const favoritesRouter = express.Router();

favoritesRouter.use(bodyParser.json());

//routing '/'
favoritesRouter.options('/', cors.corsWithOptions, (req, res) => {
	res.sendStatus(200);
});

favoritesRouter.get('/', cors.cors, authenticate.verifyUser, (req, res, next) => {
	//finds favorites by user._id
	Favorites.findOne({user: req.user._id})
	.populate('user')
	.populate('dishes')
	.then((favorites) => {
		res.statusCode = 200;
		res.setHeader('Content-Type', 'application/json');
		res.json(favorites);
	})
	.catch((err) => next(err));
});

favoritesRouter.post('/', cors.cors, authenticate.verifyUser, (req, res, next) => {
	//finds favorites by user._id
	Favorites.findOne({user: req.user._id})
	.then((favorites) => {
			//if user had no favorites - creates and save
			if(favorites == null && req.body.dishes.length !== 0) {
				Favorites.create({user: req.user._id}).
				then((favorites) =>{
						//check unique id
						req.body.dishes.forEach((dishId) => {
							if(favorites.dishes.includes(dishId._id)){
								err = new Error('Dish already in favorites');
								err.statusCode = 500;
								return next(err);
							}
							else favorites.dishes.push(dishId._id);
						});
						favorites.save()
						.then((favorites) => {
							Favorites.findById(favorites._id)
							.populate('user')
							.populate('dishes')
							.then((favorites) =>{
								res.statusCode = 200;
								res.setHeader('Content-Type', 'application/json');
								res.json(favorites);
							})
							.catch((err) => next(err));
						})
						.catch((err) => next(err));
					}
				)
				.catch((err) => next(err));
			}
			//else - just modify and save
			else {
				//check unique id
				req.body.dishes.forEach((dishId) => {
					if(favorites.dishes.includes(dishId._id)){
						err = new Error('Dish already in favorites');
						err.statusCode = 500;
						return next(err);
					}
					else favorites.dishes.push(dishId._id);
				});
				favorites.save()
				.then((favorites) => {
					Favorites.findById(favorites._id)
					.populate('user')
					.populate('dishes')
					.then((favorites) =>{
						res.statusCode = 200;
						res.setHeader('Content-Type', 'application/json');
						res.json(favorites);
					})
					.catch((err) => next(err));
				})
				.catch((err) => next(err));
			}
		}
	)
	.catch((err) => next(err));
});


favoritesRouter.delete('/', cors.cors, authenticate.verifyUser, (req, res, next) => {
	//finds favorites by user._id
	Favorites.deleteOne({user: req.user._id})
	.then((resp) =>{
		res.statusCode = 200;
		res.setHeader('Content-Type', 'application/json');
		res.json(resp);
	})
	.catch((err) => next(err));
});

//routing '/:dishId'

favoritesRouter.get('/:dishId', cors.cors, authenticate.verifyUser, (req, res, next) => {
	Favorites.findOne({user: req.user._id})
	.then((favorites) =>  {
		if(!favorites){
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			return res.json({"exists": false, "favorites": favorites});
		}
		else {
			//no such dish in favorites
			if(favorites.dishes.indexOf(req.params.dishId) < 0) {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				return res.json({"exists": false, "favorites": favorites});
			}
			else {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				return res.json({"exists": true, "favorites": favorites});
			}
		}
	}, (err) => next(err))
	.catch((err) => next(err));
});

favoritesRouter.post('/:dishId', cors.cors, authenticate.verifyUser, (req, res, next) => {
	//finds favorites by user._id
	Favorites.findOne({user: req.user._id})
	.then((favorites) =>{
		//if user had no favorites - creates and save
		if(favorites == null && req.params.dishId !== null) {
			Favorites.create({user: req.user._id}).
			then((favorites) =>{
					//check unique id
					if(favorites.dishes.includes(req.params.dishId)){
						err = new Error('Dish already in favorites');
						err.statusCode = 500;
						return next(err);
					}
					else favorites.dishes.push(req.params.dishId);
					favorites.save()
					.then((favorites) => {
						Favorites.findById(favorites._id)
						.populate('user')
						.populate('dishes')
						.then((favorites) =>{
							res.statusCode = 200;
							res.setHeader('Content-Type', 'application/json');
							res.json(favorites);
						})
						.catch((err) => next(err));
					})
					.catch((err) => next(err));
				}
			)
			.catch((err) => next(err));
		}
		//else - just modify and save
		else {
			//check unique id
			if(favorites.dishes.includes(req.params.dishId)){
				err = new Error('Dish already in favorites');
				err.statusCode = 500;
				return next(err);
			}
			else favorites.dishes.push(req.params.dishId);
			favorites.save()
			.then((favorites) => {
				Favorites.findById(favorites._id)
				.populate('user')
				.populate('dishes')
				.then((favorites) =>{
					res.statusCode = 200;
					res.setHeader('Content-Type', 'application/json');
					res.json(favorites);
				})
				.catch((err) => next(err));
			})
			.catch((err) => next(err));
		}
	})
	.catch((err) => next(err));
});


favoritesRouter.delete('/:dishId', cors.cors, authenticate.verifyUser, (req, res, next) => {
	//finds favorites by user._id
	Favorites.findOne({user: req.user._id})
	.then((favorites) =>{
		//if user had no favorites - ok
		if(favorites == null) {
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			res.json(favorites);
		}
		//else - just modify and save
		else {
			if(!favorites.dishes.includes(req.params.dishId)){
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				Favorites.findById(favorites._id)
				.populate('user')
				.populate('dishes')
				.then((favorites) =>{
					res.statusCode = 200;
					res.setHeader('Content-Type', 'application/json');
					res.json(favorites);
				})
				.catch((err) => next(err));
			}
			else favorites.dishes.remove(req.params.dishId);
			favorites.save()
			.then((favorites) => {
				Favorites.findById(favorites._id)
				.populate('user')
				.populate('dishes')
				.then((favorites) =>{
					res.statusCode = 200;
					res.setHeader('Content-Type', 'application/json');
					res.json(favorites);
				})
				.catch((err) => next(err));
			})
			.catch((err) => next(err));
		}
	})
	.catch((err) => next(err));
});

module.exports = favoritesRouter;
