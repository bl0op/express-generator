const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Leaders = require('../models/leaders')

const leaderRouter = express.Router();

leaderRouter.use(bodyParser.json());


//routing '/'
leaderRouter.options('/', cors.corsWithOptions, (req, res) => {
	res.sendStatus(200);
});

leaderRouter.get('/', cors.cors, (req, res, next) => {
		Leaders.find(req.query)
		.then((leaders) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(leaders);
		})
		.catch((err) => next(err));
	});
leaderRouter.post('/',  cors.corsWithOptions, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		var leader = new Leaders(req.body);
		leader.save()
		.then((leader) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(leader);
		})
		.catch((err) => next(err));
	});
leaderRouter.put('/',  cors.corsWithOptions, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('PUT method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});
leaderRouter.delete('/', authenticate.verifyUser, (req, res, next) => {
		Leaders.deleteMany({})
		.then((result) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(result);	
		})
		.catch((err) => next(err));
	});


//routing '/:leaderId'
leaderRouter.options('/:leaderId', cors.corsWithOptions, (req, res) => {
	res.sendStatus(200);
});

leaderRouter.get('/:leaderId', cors.cors, (req, res, next) => {
		Leaders.findById(req.params.leaderId)
		.then((leader) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(leader);	
		})
		.catch((err) => next(err));
	});
leaderRouter.post('/:leaderId',  cors.corsWithOptions, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('POST method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});
leaderRouter.put('/:leaderId', authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		Leaders.findByIdAndUpdate(req.params.leaderId,{
				$set: req.body
			}, { new: true }
		)
		.then((leader) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(leader);	
		})
		.catch((err) => next(err));
	});
leaderRouter.delete('/:leaderId',  cors.corsWithOptions, authenticate.verifyUser,   authenticate.verifyAdmin, (req, res, next) => {
		Leaders.findByIdAndDelete(req.params.leaderId)
		.then((leader) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(leader);	
		})
		.catch((err) => next(err));
	});

module.exports = leaderRouter;
