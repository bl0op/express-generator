const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Promotions = require('../models/promotions')

const promoRouter = express.Router();

promoRouter.use(bodyParser.json());


//routing '/'
promoRouter.options('/', cors.corsWithOptions, (req, res) => {
	res.sendStatus(200);
});

promoRouter.get('/', cors.cors, (req, res, next) => {
		Promotions.find(req.query)
		.then((promos) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(promos);
		})
		.catch((err) => next(err));
	});
promoRouter.post('/', cors.corsWithOptions, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		var promo = new Promotions(req.body);
		promo.save()
		.then((promo) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(promo);
		})
		.catch((err) => next(err));
	});
promoRouter.put('/', cors.corsWithOptions, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('PUT method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});
promoRouter.delete('/',  cors.corsWithOptions, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		Promotions.deleteMany({})
		.then((result) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(result);	
		})
		.catch((err) => next(err));
	});


//routing '/:promoId'
promoRouter.get('/:promoId', cors.cors, (req, res, next) => {
		Promotions.findById(req.params.promoId)
		.then((promo) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(promo);	
		})
		.catch((err) => next(err));
	});
promoRouter.post('/:promoId', cors.corsWithOptions,  authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('POST method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});
promoRouter.put('/:promoId', cors.corsWithOptions,  authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		Promotions.findByIdAndUpdate(req.params.promoId,{
				$set: req.body
			}, { new: true }
		)
		.then((promo) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(promo);	
		})
		.catch((err) => next(err));
	});
promoRouter.delete('/:promoId', cors.corsWithOptions,  authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
		Promotions.findByIdAndDelete(req.params.promoId)
		.then((promo) => {
			res.statusCode = 200;
			res.setHeader('Content-type', 'application/json');
			res.json(promo);	
		})
		.catch((err) => next(err));
	});

module.exports = promoRouter;
