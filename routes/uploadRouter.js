const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const multer = require('multer');
const cors = require('./cors');

var storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, 'public/images');
	},

	filename: (req, file, cb) => {
		cb(null, file.originalname);
	}
});

const imageFileFilter = (req, file, cb) => {
		if(!file.originalname.match(/\.(jpg|jpef|png|gif)$/)){
			return cb(new Error('You can upload only image files!'));
		}
	cb(null, true);
};

const upload = multer({ storage: storage, fileFilter: imageFileFilter});

const uploadRouter = express.Router();

uploadRouter.use(bodyParser.json());


//routing '/'
uploadRouter.options('/', cors.corsWithOptions, (req, res) => {
	res.sendStatus(200);
});

uploadRouter.get('/', cors.cors, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('GET method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});
	
uploadRouter.post('/',  cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, upload.single('imageFile'), (req, res) => {
		res.statusCode = 200;
		res.setHeader('Content-Type', 'application/json');
		res.json(req.file);
	});

uploadRouter.put('/',  cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('PUT method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});

uploadRouter.delete('/',  cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
		res.statusCode = 403;
		res.end('DELETE method is not available on url: ' + req.protocol + '://' + req.get('host') + req.originalUrl);
	});

module.exports = uploadRouter;
